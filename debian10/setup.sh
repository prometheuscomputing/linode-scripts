#!/bin/bash

# You must set these manually.
# - USERNAME
# - USERPUBKEY
# - HOSTNAME
# - WEBSERVERPRIVATEKEY # this hasn't been working so is currently not required.  You must set up this key manually

# set -x # may want to turn this on for debugging....

wget -q https://gitlab.com/prometheuscomputing/linode-scripts/-/raw/master/setup_functions.sh
source ./setup_functions.sh

export WEBSERVERPUBLICKEY="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDDkf3POYAtrk07Sn+pXmDPSrIkNs5ClU+bJ647uw8R4HnsgSMJSVXyl124APQAEG2XbC3wb83VmeFCqgNEmEU8mkoEcWFwnqwTV+Cg8hMBZrcqTT0WL+YFL/6ifjGTBm7SIj07T3EjffnX4c3fjR/5NBH/iJQUrZdewsCkzS1uYrZ2d73p6XMFtfbQnVXaXq5I+S8L2HwVYMkfdSoxnDlmVoOKxNrMCr1kMCUpm/ar1ITAb0YhKavthBuJp17TdJejQeFwLftm/eX+LWwe+EETrf+8G61gV2Om6bt9+OcBWMF2KYUAbj4Lr3eA544QWl4fiO6+bqq8D0VWEp9f+UWt webserver@localhost"

# Setup script
system_set_timezone "America/New_York"

groupadd rvm

adduser --disabled-password --gecos "" $USERNAME
usermod -aG sudo,rvm $USERNAME
user_add_pubkey $USERNAME "$USERPUBKEY"
enable_passwordless_sudo $USERNAME

adduser --disabled-password --gecos "" webserver
usermod -aG sudo,rvm webserver
# user_add_pubkey webserver "$WEBSERVERPUBLICKEY"
# setup_webserver_private_key "$WEBSERVERPRIVATEKEY"

# update / upgrage everything
dfapt update
dfapt "upgrade --with-new-pkgs"
dfapt dist-upgrade
dfapt autoremove

# install utilities
dfaptinstall "zip unzip"

# Install libcurl (used by curb gem)
dfaptinstall "libcurl4-openssl-dev"

# Install Nginx
dfaptinstall "nginx"
rm /etc/nginx/sites-enabled/default

# Install snap
dfaptinstall snapd

# Install CertBot (for LetsEncrypt)
snap install --classic certbot
ln -s /snap/bin/certbot /usr/bin/certbot

# Install RVM in multi-user mode using Curl (Don't use ubuntu_rvm package as it installs in mixed mode)
gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
\curl -sSL https://get.rvm.io | sudo bash -s stable
# RVM initialization for this SSH session (will be automatic for future logins)
source /etc/profile.d/rvm.sh

# Install Ruby -- NOTE: we are currently stalled at Ruby 2.6.3 due to compatibility issues.  Change this when we know that we have resolved issues
rvm autolibs enable
rvm install ruby-2.7.1

# Switch to webserver
su - webserver

# Configure Ruby gems (double check this part!)
gem sources -a 'https://linode-public:!Ep!meTh3us!@gems.prometheuscomputing.com'
echo 'gem: "--no-document"' >> ~/.gemrc

# Exit webserver user
exit

# Make .gemrc into system-wide configuration
mv /home/webserver/.gemrc /etc/gemrc

# Install Rustup
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y

# Install exa
# Check for newer version.  If exists, update url and exazip
exazip="exa-linux-x86_64-0.9.0.zip" 
url="https://github.com/ogham/exa/releases/download/v0.9.0/$exazip"
wget -c $url
unzip $exazip
mv exa-linux-x86_64 /usr/local/bin/exa
rm $exazip

system_set_hostname $HOSTNAME

# Cron jobs???????????????? FIXME!
# - Rotate logs?
# - Backup DBs? (find rotate-daily script)

ssh_disable_root
set +x # in case you set -x earlier
echo "setup script complete" > /root/success.txt
